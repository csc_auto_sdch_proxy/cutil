package storage_test

import (
	"bytes"
	"os"
	"testing"

	"bitbucket.com/csc_auto_sdch_proxy/cocaineutils/storage"
)

var (
	endpoint string
)

func init() {
	endpoint = os.Getenv("COCAINE_ENPOINT")
	if len(endpoint) == 0 {
		endpoint = storage.DefaultEndpoint
	}
}

func initializeStorage(t *testing.T) storage.Storage {
	s, err := storage.New("testnamespace", endpoint)
	if err != nil {
		t.Fatalf("Unexpected creation error: %s", err)
	}
	return s
}

func TestInitializeStorage(t *testing.T) {
	_ = initializeStorage(t)
	_ = initializeStorage(t)
}

func TestWriteNotFails(t *testing.T) {
	s0 := initializeStorage(t)
	s1 := initializeStorage(t)
	for i, s := range []storage.Storage{s0, s1} {
		key := "writeTestKey"
		data := []byte("writeTestData")
		err := s.Write(key, data)
		if err != nil {
			t.Errorf("Write error on storage %d: %s", i, err)
		}
	}
}

func TestNoKeyReadFails(t *testing.T) {
	s := initializeStorage(t)
	key := "notDataOnThisKeyReadTest"
	blob, err := s.Read(key)
	if err != nil {
		t.Logf("OK - 'no shuch key' read failed with error: %s", err)
	} else {
		t.Errorf("Read failed on key without data not failed.")
	}

	if blob != nil {
		t.Errorf("Read failed. Blob should be nil, but got: \"%s\"", string(blob))
	}
}

func TestWriteResultRead(t *testing.T) {
	s0 := initializeStorage(t)
	key := "writeTestKey"
	writeBlob := []byte("writeTestData")
	if err := s0.Write(key, writeBlob); err != nil {
		t.Fatalf("Write failed: %s", err)
	}
	s1 := initializeStorage(t)
	for _, s := range []storage.Storage{s0, s1} {
		if readBlob, err := s.Read(key); err == nil {
			if !bytes.Equal(writeBlob, readBlob) {
				t.Errorf(
					"Writed blob and readed are not equal. Writed: \"%s\". Readed: \"%s\".",
					string(writeBlob),
					string(readBlob),
				)
			}
		} else {
			t.Errorf("Read writed error: %s", err)
		}
	}
}

func TestRemoveNotFails(t *testing.T) {
	s := initializeStorage(t)
	keyNoData := "notDataOnThisKeyRemoveTest"
	keyWrited := "writeTestKey"
	writeBlob := []byte("writeTestData")

	if err := s.Remove(keyNoData); err != nil {
		t.Errorf("Remove failed on key without data on it: %s", err)
	}

	if err := s.Write(keyWrited, writeBlob); err != nil {
		t.Fatalf("Write failed: %s", err)
	}

	if err := s.Remove(keyWrited); err != nil {
		t.Errorf("Remove failed on key with data on it: %s", err)
	}

}

func TestReadFailsAfterRemove(t *testing.T) {
	s := initializeStorage(t)
	key := "writeTestKey"
	writeBlob := []byte("writeTestData")

	if err := s.Write(key, writeBlob); err != nil {
		t.Fatalf("Write failed: %s", err)
	}

	if err := s.Remove(key); err != nil {
		t.Fatalf("Remove failed on key with data on it: %s", err)
	}

	blob, err := s.Read(key)
	if err != nil {
		t.Logf("OK - 'no shuch key' read failed with error: %s", err)
	} else {
		t.Errorf("Read failed on key without data not failed.")
	}
	if blob != nil {
		t.Errorf("Read failed. Blob should be nil, but got: \"%s\"", string(blob))
	}

}
