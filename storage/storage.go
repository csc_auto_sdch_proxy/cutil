package storage

import (
	"github.com/cocaine/cocaine-framework-go/cocaine"
)

const (
	DefaultNamespace = ""
	DefaultEndpoint  = "localhost:10053"
)

var (
	noTags = make([]string, 0)
)

type Storage interface {
	Read(key string) ([]byte, error)
	Write(key string, blob []byte) error
	Remove(key string) error
}

type storage struct {
	s         *cocaine.Service
	namespace string
}

func New(namespace string, endpoint string) (Storage, error) {
	s, err := cocaine.NewService("storage", endpoint)
	if err != nil {
		return nil, err
	}
	return &storage{s, namespace}, nil
}

func NewDefault() (st Storage, err error) {
	return New(DefaultNamespace, DefaultEndpoint)
}

func (s *storage) Read(key string) (blob []byte, err error) {
	res := <-s.s.Call("read", s.namespace, key)
	err = res.Err()
	if err != nil {
		return
	}
	res.Extract(&blob)
	return
}

func (s *storage) Write(key string, blob []byte) (err error) {
	res, opened := <-s.s.Call("write", s.namespace, key, blob, noTags)
	if opened {
		err = res.Err()
	}
	return

}

func (s *storage) Remove(key string) (err error) {
	res, opened := <-s.s.Call("remove", s.namespace, key)
	if opened {
		err = res.Err()
	}
	return

}
