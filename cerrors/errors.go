package cerrors

import (
	"encoding/json"
	"fmt"
	"net/http"

	"errors"

	"runtime/debug"

	"bitbucket.com/csc_auto_sdch_proxy/cutil"
)

type Error interface {
	error
	StatusCode() int
	Name() string
}

func StatusCode(err error) int {
	if sErr, ok := err.(Error); ok {
		return sErr.StatusCode()
	}
	return http.StatusInternalServerError
}

func Name(err error, defName string) string {
	if sErr, ok := err.(Error); ok {
		return sErr.Name()
	}
	return defName
}

func String(err error) string {
	if sErr, ok := err.(Error); ok {
		return fmt.Sprintf("Error: %s\n Description: %s", sErr.Name(), sErr.Error())
	}
	return err.Error()

}

func RecoverToErr(r interface{}) error {
	if err, ok := r.(error); ok {
		return err
	}

	return &CustomInternal{fmt.Sprintf("Unrecognised panic: %v", r), errors.New(string(debug.Stack()))}
}

// ErrHandler should serve HTTP request or return error
// error should be returned before status code set or any write actions with ResponseWriter
// if error happens after any write actions with ResponseWriter, ErrHandler should handle it itself and return nil
// if ServeOrErr return non nil error callee should serve request itself
type ErrHandler interface {
	ServeOrErr(http.ResponseWriter, *http.Request) error
}

type LogHandler interface {
	cutil.Logger
	ErrHandler
}

type HTTPHandler struct {
	LogHandler
}

func WriteJSON(w http.ResponseWriter, err error, log cutil.Logger) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(StatusCode(err))
	if jsonErr := json.NewEncoder(w).Encode(map[string]string{
		"error":       Name(err, "error"),
		"description": err.Error(),
	}); jsonErr != nil {
		log.Errf("json error response error: %s", jsonErr)
	}
}

func (h *HTTPHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if err := h.ServeOrErr(w, r); err != nil {
		h.Errf("HTTP serve failed with error: %s", err)
		WriteJSON(w, err, h)
	}

}

type CustomInternal struct {
	CustomName string
	Cause      error
}

func (err *CustomInternal) Name() string    { return err.CustomName }
func (err *CustomInternal) StatusCode() int { return http.StatusInternalServerError }
func (err *CustomInternal) Error() string {
	if err.Cause == nil {
		return err.CustomName
	}
	return fmt.Sprintf("%s: %s", err.CustomName, err.Cause)
}

type CustomClient struct {
	CustomName string
	Cause      error
}

func (err *CustomClient) Name() string    { return err.CustomName }
func (err *CustomClient) StatusCode() int { return http.StatusBadRequest }
func (err *CustomClient) Error() string {
	if err.Cause == nil {
		return err.CustomName
	}
	return fmt.Sprintf("%s: %s", err.CustomName, err.Cause)
}

type ResponseEncode struct{ Cause error }

func (err *ResponseEncode) Name() string    { return "response encode" }
func (err *ResponseEncode) StatusCode() int { return http.StatusInternalServerError }
func (err *ResponseEncode) Error() string   { return err.Cause.Error() }

type RequestDecode struct{ Cause error }

func (err *RequestDecode) Name() string    { return "request decode" }
func (err *RequestDecode) StatusCode() int { return http.StatusBadRequest }
func (err *RequestDecode) Error() string   { return err.Cause.Error() }

type UnsupportedMediaType struct{ UnsupportedType string }

func (err *UnsupportedMediaType) Name() string    { return "unsupported media type" }
func (err *UnsupportedMediaType) StatusCode() int { return http.StatusUnsupportedMediaType }
func (err *UnsupportedMediaType) Error() string {
	return fmt.Sprintf("unsuported media type: %s", err.UnsupportedType)
}

type UnsupportedMethod struct{ UnsupportedMethod string }

func (err *UnsupportedMethod) Name() string    { return "Unsupported Method Error" }
func (err *UnsupportedMethod) StatusCode() int { return http.StatusMethodNotAllowed }
func (err *UnsupportedMethod) Error() string {
	return fmt.Sprintf("Unsuported method: %s", err.UnsupportedMethod)
}
